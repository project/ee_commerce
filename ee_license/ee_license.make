api = 2
core = 7.x

defaults[projects][subdir] = contrib

projects[commerce_license][version] = 1.3
projects[commerce_license_billing][version] = 1.0-beta4
projects[entity_bundle_plugin][version] = 1.0-beta2
