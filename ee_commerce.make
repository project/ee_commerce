api = 2
core = 7.x

defaults[projects][subdir] = contrib

projects[commerce][version] = 1.14

; Backoffice

projects[commerce_backoffice][version] = 1.5

; Performance

projects[commerce_authcache][version] = 1.0-beta1
projects[commerce_entitycache][version] = 1.2

; Search API

projects[commerce_search_api][version] = 1.4
projects[commerce_search_api_et][type] = module
projects[commerce_search_api_et][download][type] = git
projects[commerce_search_api_et][download][revision] = 504f9f4eff7abb5b16bb5c31ab4e6cb01b2024fd

; Sub Modules

includes[ee_commerce_file] = ee_commerce_file/ee_commerce_file.make
includes[ee_license] = ee_license/ee_license.make
includes[ee_license_role] = ee_license_role/ee_license_role.make
